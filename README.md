# EJABBERD A XMPP SERVER

Um was geht es bei eJabberd. eJabberd stellt ein dezentrales Ökostem über das Protokoll XMPP zur Verfügung.
User Accounts werden in Form einer Mailadresse angelegt ( [user@example.org](mailto:user@example.org) )
Der Server eines Users der Domaine A versucht bei der Initialiseirung eines Chats des Users auf Domain B diese zu fragen, ob es diesen User dort gibt.
Ist dies der Fall, wird einen Verbindung zwischen beiden Users über die entsprechenden Server hergestellt.

Um uns nun unabhängig von gängigen Regularien zu machen, wollen wir Nachfolgend die Umsetzung und den Betrieb auf eigener K8s Infrastruktur beschreiben.
Vorraussetzung ist hier ein bereits laufender k8s Cluster.
Es darf auch microk8s sein. Beide sind manifest - kompatibel.
Der Umgang mit beiden Technologieen sollte vertraut sein.

Als Reverseproxy kommt <https://traefik.io/traefik/> zu Einsatz.

Als Server <https://www.ejabberd.im/index.html>

## QUELLEN

- [ ] <https://docs.ejabberd.im/tutorials/mysql/>
- [ ] <https://docs.ejabberd.im/admin/configuration/database/>
- [ ] <https://docs.ejabberd.im/admin/guide/security/>
- [ ] <https://github.com/processone/ejabberd/tree/23.10/sql>
- [ ] <https://www.kuketz-blog.de/ejabberd-installation-und-betrieb-eines-xmpp-servers/>
- [ ] Danke an Gerd für die Initalidee zu diesem Projekt und die immer sehr guten Tipps.

# AUFBAU DES REPOS

## configmap/configmap.yaml

Beinhaltet den kompletten Configfile für ejabberd

## middleware/ejabberd-replace.yaml

Ist eine traefik ingress middleware für das webadmin interface, welche den AdminPfad /admin nach / umleitet

## ingressv2.yaml

Ist die komplette Ingress Regel für traefik. \\
Hier werden alle Routen (TCP/UDP) für die Funktionalität von ejabberd definiert.

## ingressv2_admin.yaml

![Screenshot_20241024_145339.png](images/Screenshot_20241024_145339.png)

Ist das Admin Interface, welches nur von intern erreichbar sein sollte  
Hierzu ist noch nötig Deine internen Certifiakte, welche Du hoffentlich bereits schon als Secret in k8s hinterlegt hast, einzutragen.

```bash
SECRETNAME_INTERNAL_CERT="<your-internal-tls-domain-secret-name>"

find . -type f -exec sed -i s/\{\{\ WILDCARD.INTERNAL.DOMAIN\ \}\}/${}/g {SECRETNAME_INTERNAL_CERT} \;
```

## ingressv2_conversejs.yaml

Ist der eigebaute converse WebChat Client. Extern verfügbar, Accounterstellung deaktivert.

![Screenshot_20241024_145455.png](images/Screenshot_20241024_145455.png)

### WICHTIG

Alle Ingress manifest beziehen sich auf **traefik 2.11.x** Es exisitert ein Unterschied zischen traefik 2.9.xx zu traefik 2.11.xx

- [ ] traefik > 2.10 | apiVersion: [traefik.io/v1alpha1](http://traefik.io/v1alpha1)
- [ ] traefik < 2.10 | apiVersion: [traefik.containo.us/v1alpha1](http://traefik.containo.us/v1alpha1)

## storage.yaml

Erzeugt alle pesistenten Soragepfade, welche von ejabberd und mariadb benötigt werden.
Es wird der default Provisioner verwendet.

## ejabberd.yaml

Die komplette Service und deployment Beschreibung, welche für das hochziehen des Pods nötig ist.

**Der Pod beinhaltet:**

- ejabberd
- mariadb

# ANPASSUNG AUF DEINE UMGEBUNG

## 1. STEP

### NAMESPACE

Der definierte Namespace in den Manifests. Ihr müsst diesen anpassen.

```bash
YOUR_NEW_NAMESPACE="xxxxx"

find . -type f -exec sed -i s/\{\{\ .NAMESPACE\ \}\}/${YOUR_NEW_NAMESPACE}/g {} \;
```

## 2. STEP

Alle weiteren Dateien müssen nun, für Deine Bedürfnisse noch angepasst werden.

**DB Passworte ändern.**

Ersetze **<abc123>** durch Dein neues DB-User-Passwort  
Ersetze **<cde456>** durch Dein neues DB-Root_Passwort

```bash
YOUR_NEW_USER_PASSWORD="<abc123>"
YOUR_NEW_ROOT_PASSWORD="<cde456>"

find . -type f -exec sed -i s/\{\{\ .MARIADB.USER.PASSWORD\ \}\}/${YOUR_NEW_USER_PASSWORD}/g {} \;

## DAS SELBE AUCH FÜR DAS ROOT - PASSWORD
find . -type f -exec sed -i s/\{\{\ .MARIADB.ROOT.PASSWORD\ \}\}/${YOUR_NEW_ROOT_PASSWORD}/g {} \;
```

Damit wäre dieser Schritt abgeschlossen. Es wurde Dein neues Passowort in allen Dateien geändert.

## 3. STEP

Deine Domain muss hinterlegt werden.

Du musst diese ändern.

Ersetze **<example.domain>** mit Deiner Richtigen **Domain** **(extern)**  
Ersetze **<1.2.3.4>** mit Deiner offiziellen **IPv4 (extern)**

```bash
YOUR_DOMAIN="<example.domain>" YOUR_EXTERNAL_IP="<1.2.3.4>"  find . -type f -exec sed -i s/\{\{\ .EXAMPLE.DOMAIN\ \}\}/${YOUR_DOMAIN}/g {} \;  ## ANPASSEN DEINER EXTERNEN IP find . -type f -exec sed -i s/\{\{\ .EXTERNAL.IP \}\}/${YOUR_EXTERNAL_IP}/g {} \;
```

## 4. STEP

DNS - Records anlegen. Alternativ können es auch CNAME Records sein. welche auf eine Doamin zeigen.

```dns
pubsub     IN A YOUR_EXTERNAL_IP
upload     IN A YOUR_EXTERNAL_IP
proxy      IN A YOUR_EXTERNAL_IP
conference IN A YOUR_EXTERNAL_IP
```

| NAME        | TYPE | VALUE          | PRIORITY | PORT |
|-------------|------|----------------|----------|------|
| *stun.*udp  | SRV  | example.domain | 0        | 3478 |
| *stuns.*tcp | SRV  | example.domain | 0        | 5349 |
| *turn.*udp  | SRV  | example.domain | 0        | 3478 |
| *turns.*tcp | SRV  | example.domain | 0        | 5349 |

## 5. STEP

Übertragung der Configruarion des traefik Reverseproxies unter  traefik/traefik.toml
in Deine bestehende traefik.toml. Wenn Du Deinen Traefik als Container deployed hast, musst Du die .toml Datei in das .yaml Format umwandeln zB

- [ ] <https://transform.tools/toml-to-yaml>

### Nicht vergessen folgende Ports auf Deinem Externen Host frei zu schalten:

**TCP** :: 80 443 3478 5222 5223 5269 5270 5349 5443 **UDP** :: 3478 49152 -> 49155

Welche Verwendung diese Ports haben, bitten wir unter <https://docs.ejabberd.im/admin/> nachzulesen.

### IPTABLES BSP

```cli
IPTABLES="/sbin/iptables
#...
$IPTABLES -A INPUT -p tcp -m multiport  --dports 80,443,3478,5222,5223,5269,5270,5349,5443 -j ACCEPT
$IPTABLES -A INPUT -p udp -m multiport  --dports 3478,49152:49155 -j ACCEPT
#...
```

| **PORT**        | **UDP** | **TCP** |
|-------------|:---:|:---:|
| 5222        |     |  x  |
| 5223        |     |  x  |
| 5269        |     |  x  |
| 5270        |     |  x  |
| 5280        |     |  x  |
| 5349        |     |  x  |
| 8080        |     |  x  |
| 3478        |  x  |  x  |
| 49152-49155 |  x  |     |
| 5349        |     |  x  |

## 6. STEP

### Create a Secret in Kubernetes

um die Zertifikate, welche für den Betrieb von ejabberd erforderlich sind, in Kubernetes hinzuzufügen, müssen zuerst die benötigen Zertifikate auf dem externen Host unter Verwendung von Letsencrypt Certbot erstellt werden.  Wie man Certbot einsetzt müsst ihr bitte zB hier <https://min.io/docs/minio/linux/integrations/generate-lets-encrypt-certificate-using-certbot-for-minio.html> nachlesen. Der Vereinfachung im folgenden ein Befehl, mit dem man die entsprechenden Certifikate erstellen kann

```bash
certbot certonly -d example.domain -d conference.example.domain -d proxy.example.domain -d upload.example.domain -d pubsub.example.domain
```

Die erzeugte Chain kopiert Ihr bitte in das secret Verzeichnis und führt das Script **create\_[secret.sh](http://secret.sh)** aus.

```cli
kubectl create secret tls xmpp-fullchain --cert=fullchain.pem --key=privatekey.pem -n <YOUR_NAMESPACE>
```

Das erzeugte Secret heisst nun **xmpp-fullchain**

```cli
└─$ kubectl get secret -n <YOUR_NAMESPACE> xmpp-fullchain
NAME                     TYPE                DATA   AGE
xmpp-fullchain   kubernetes.io/tls   2      2d21h

└─$ kubectl describe secret -n public xmpp-fullchain
Name:         xmpp-fullchain
Namespace:    public
Labels:       <none>
Annotations:  <none>

Type:  kubernetes.io/tls

Data
====
tls.crt:  3668 bytes
tls.key:  1704 bytes
```

**lts.crt** und **tls.key** werden nun bein start in **STEP 6** über ejabberd.yaml an die entsprechende Stelle gemounted die in  **configmap/configmap.yaml**
definiert wurde

```cli
    ## If you already have certificates, list them here
    certfiles:
      - /etc/letsencrypt/live/domain.tld/tls.crt
      - /etc/letsencrypt/live/domain.tld/tls.key
```

## 7. STEP

Ist bis hierher alles geschafft, starten wir den Pod

```yaml
kubectl apply -f configmap/configmap.yaml
kubectl apply -f middleware/ejabberd-replace.yaml 
kubectl appyl -f middleware/ejabberd-security.yaml 
kubectl apply -f .
```

## 8. STEP

### Installation des DB - Schemas

```cli
kubectl exec -it ejabberd-66b5bbbddf-r45qh -n public -c mariadb -- bash

root@ejabberd:/# env | grep MARIADB
MARIADB_ROOT_PASSWORD=xxxxxxxxxxxxxxxxxxxxxxxxxx
MARIADB_PASSWORD=ieboo2Ut4ooQueeC8Uajiebiel6eekoo
MARIADB_USER=ejabberd
MARIADB_DATABASE=ejabberd


root@ejabberd:/# apt update
root@ejabberd:/# apt install wget 

root@ejabberd:/# wget https://github.com/processone/ejabberd/blob/master/sql/mysql.new.sql

root@ejabberd:/# mysql -u${MARIADB_USER} -p${MARIADB_PASSWORD} ${MARIADB_DATABASE} < mysql.new.sql
```

## 9. Step

### Admin User Erzeugen

Um das System überhaupt erst nutzen zu können, müsst ihr nun noch einen AdminUser anlegen. Als Ausgangpunkt verwenden wir  
{{ .[USER.NAME](http://USER.NAME) }}@{{ .EXAMPLE.DOMAIN }}  
Diese beiden Werte könnt ihr beispiuelhaft nun wie folgt ersetzen:  
**{{ .[USER.NAME](http://USER.NAME) }} == admin**  
**{{ .EXAMPLE.DOMAIN }} == your.externe.domain** ( = wie weiter oben schon in den Manifests angelegt )

Zunächst ersten wir in den Manifest nochmals den Usernamen mit einem von Dir gewählten.

```bash
## ANPASSEN DES ADMIN-USERKONTOS (beispielhaft)
YOUR_USER_NAME="admin"

find . -type f -exec sed -i s/\{\{\ .USER.NAME \}\}/${YOUR_USER_NAME}/g {} \;
```

Nun erzeugen wir den selben User innerhalb des pods unter Verwendung des Befehls ejabberdctl den selben User.

```cli
~ $ cd /home/ejabberd/bin/
~/bin $ ./ejabberdctl register admin "your.external.domain" "SomEPass44"
```

# NÜTZLICHE ZUSATZINFOS

## Container selber bauen

Sollte Ihr nicht das in ejabberd eingetragene repository verwenden wollen sondern Euren eigenen Container bauen, müsst Ihr den Anweisungen unter folgender Seite folgen:

- \[\] <https://github.com/processone/docker-ejabberd/tree/master/ecs#readme>

Danach funktioniert **captcha nicht** da einige Pakte fehlen, weshalb ihr den Dockerfile, wie unten beschrieben anpassen müßt.

## Dockerfile Modification

Um **captcha** nutzen zu können, müssen zwei Pakte nachinstalliert werden
**Imagemagic** und **ghostscript-fonts**

```Bash
...
# Install required dependencies
RUN apk upgrade --update-cache --no-progress \
    && apk add \
    expat \
    freetds \
    gd \
    jpeg \
    libgd \
    libpng \
    libstdc++ \
    libwebp \
    ncurses-libs \
    openssl \
    sqlite \
    sqlite-libs \
    tini \
    unixodbc \
    yaml \
    zlib \
    imagemagick \
    ghostscript-fonts \
    && ln -fs /usr/lib/libtdsodbc.so.0 /usr/lib/libtdsodbc.so \
    && rm -rf /var/cache/apk/*
...
```
