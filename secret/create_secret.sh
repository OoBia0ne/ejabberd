#!/bin/bash

kubectl create secret tls xmpp-fullchain --cert=fullchain.pem --key=privatekey.pem -n {{ .NAMESPACE }}

